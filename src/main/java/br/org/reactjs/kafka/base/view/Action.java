package br.org.reactjs.kafka.base.view;

import static br.org.reactjs.kafka.base.constants.Constants.RENDER;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

/**
 * Anotação para ações de controllers.
 * 
 * @author Theodoro Mattoso
 * @since 19/12/2020
 */
@Target(METHOD)
@Retention(RUNTIME)
public @interface Action {

  /**
   * Identificação da ação da tela.
   * 
   * @return {@link Integer} id da tela.
   */
  public int id() default RENDER;

}