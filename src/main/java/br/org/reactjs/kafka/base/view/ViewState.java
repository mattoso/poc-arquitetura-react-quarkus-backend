package br.org.reactjs.kafka.base.view;

import java.io.Externalizable;

/**
 * Classe de base para todos os objetos de controle de estado das telas.
 * 
 * @author Theodoro Mattoso
 * @since 19/12/2020
 */
public interface ViewState {

	String getViewId();
	
	void setViewId(final String viewId);
}