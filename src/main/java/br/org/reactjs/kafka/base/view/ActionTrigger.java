package br.org.reactjs.kafka.base.view;

/**
 * DTO de gatilho de ações do frontend.
 * 
 * @author Theodoro Mattoso
 * @since 26/12/2020
 */
public class ActionTrigger {

	private String viewId;
	private int actionId;
	/**
	 * @return the viewId
	 */
	public String getViewId() {
		return viewId;
	}
	/**
	 * @param viewId the viewId to set
	 */
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}
	/**
	 * @return the actionId
	 */
	public int getActionId() {
		return actionId;
	}
	/**
	 * @param actionId the actionId to set
	 */
	public void setActionId(int actionId) {
		this.actionId = actionId;
	}
}