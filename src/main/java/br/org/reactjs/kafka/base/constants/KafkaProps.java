package br.org.reactjs.kafka.base.constants;

import java.util.Properties;

import io.quarkus.arc.config.ConfigProperties;

@ConfigProperties(prefix = "kafka")
public class KafkaProps {

	private String server;

	/**
	 * @return the bootstrapServers
	 */
	public String getServer() {
		return server;
	}

	/**
	 * @param bootstrapServers the bootstrapServers to set
	 */
	public void setServer(String server) {
		this.server = server;
	}
	
	public Properties getProps() {
		final var props = new Properties();
		props.put("bootstrap.servers", this.server);
		props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
		return props;
	}
	
}
