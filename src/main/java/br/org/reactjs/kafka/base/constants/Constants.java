package br.org.reactjs.kafka.base.constants;

/**
 * Constantes de base do sistema.
 * 
 * @author Theodoro Mattoso
 * @since 20/12/2020
 */
public final class Constants {
	private Constants() {
		throw new IllegalArgumentException("Esta classe não pode produzir instâncias dela mesma.");
	}
	public static final int RENDER = 0;
	public static final String VIEW_STATES = "ViewStates";
}