package br.org.reactjs.kafka.base.controller;

import br.org.reactjs.kafka.base.view.ViewState;

/**
 * Base do controlador de telas. Utiliza o {@link ViewState} para submeter ações
 * e atualizações de estado.
 * 
 * @author Theodoro Mattoso
 * @since 19/12/2020
 */
public interface Controller<V extends ViewState> {

	/**
	 * Ação padrão para todos so controllers, toda vez que uma tela finaliza sua
	 * renderização, o método componentDidMount irá postar uma mensagem na fila de
	 * ações para que o controller carregue o objeto que representa o state, podendo
	 * ser uma ação em base de dados, consulta à apis, etc.
	 */
	void render();

	void updateState();
	
	/**
	 * Retorna o {@link ViewState} controlado pelo controlador.
	 * @return {@link ViewState}
	 */
	V getViewState();

}