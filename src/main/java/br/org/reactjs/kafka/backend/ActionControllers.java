package br.org.reactjs.kafka.backend;

import java.util.Map;
import java.util.TreeMap;

import br.org.reactjs.kafka.base.controller.Controller;
import br.org.reactjs.kafka.base.view.ViewState;

/**
 * Singleton que armazena os controladores.
 * 
 * @author Theodoro Mattoso
 * @since 26/12/2020
 */
public enum ActionControllers {

	ACTION_CONTROLLERS;
	
	private Map<String, Controller<? extends ViewState>> controllers = new TreeMap<>();
	
	public void registerController(final Controller<? extends ViewState> controller) {
		this.controllers.put(controller.getViewState().getViewId(), controller);
	}
	
	public Map<String, Controller<? extends ViewState>> getControllers() {
		return this.controllers;
	}
}