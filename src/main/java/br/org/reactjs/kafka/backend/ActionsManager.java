package br.org.reactjs.kafka.backend;

import static br.org.reactjs.kafka.backend.ActionControllers.ACTION_CONTROLLERS;
import static java.util.Arrays.asList;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import br.org.reactjs.kafka.base.view.Action;
import br.org.reactjs.kafka.base.view.ActionTrigger;

@ApplicationScoped
public class ActionsManager {

	@Incoming("view-actions")
	public void onAction(final ActionTrigger action) {
		final var controller = ACTION_CONTROLLERS.getControllers().get(action.getViewId());
		try {
			asList(controller.getClass().getDeclaredMethods())
				.stream()
				.filter(m -> m.isAnnotationPresent(Action.class))
				.filter(m -> m.getAnnotation(Action.class).id() == action.getActionId())
				.iterator()
				.next()
				.invoke(controller);
		} catch(final Exception e) {
		}
	}
}