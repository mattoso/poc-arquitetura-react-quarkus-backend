package br.org.reactjs.kafka.backend.controller;

import static br.org.reactjs.kafka.base.constants.Constants.VIEW_STATES;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;

import br.org.reactjs.kafka.base.constants.KafkaProps;
import br.org.reactjs.kafka.base.controller.Controller;
import br.org.reactjs.kafka.base.view.Action;
import br.org.reactjs.kafka.frontend.states.HomeViewState;

public class HomePageController implements Controller<HomeViewState> {

	private static final Logger LOGGER = LoggerFactory.getLogger(HomePageController.class);
	
	private KafkaProps kafkaProps;
	private HomeViewState viewState;
	
	private HomePageController(HomeViewState homeViewState, KafkaProps props) {
		this.viewState = homeViewState;
		this.kafkaProps = props;
	}

	public static HomePageController initialize(final KafkaProps props) {
		return new HomePageController(new HomeViewState(), props);
	}
	
	@Action
	@Override
	public void render() {
		this.viewState.setColor("#ff0000");
		this.updateState();
	}

	@Override
	public void updateState() {
		final String stateJson = new Gson().toJson(this.viewState);
		try (final var producer = new KafkaProducer<String, String>(this.kafkaProps.getProps())){
			producer.send(new ProducerRecord<String, String>(VIEW_STATES, stateJson));
		} finally {
			LOGGER.info("VIEWSTATE ATUIALIZADO -> {}", stateJson);
		}
	}

	@Override
	public HomeViewState getViewState() {
		return this.viewState;
	}
}