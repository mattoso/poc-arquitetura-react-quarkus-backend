package br.org.reactjs.kafka.frontend;

import br.org.reactjs.kafka.base.view.ActionTrigger;
import io.quarkus.kafka.client.serialization.ObjectMapperDeserializer;

/**
 * Deserializador de objetos de ação performada.
 * 
 * @author Theodoro Mattoso
 * @since 26/12/2020
 */
public class ActionsDeserializer extends ObjectMapperDeserializer<ActionTrigger> {

	public ActionsDeserializer() {
		super(ActionTrigger.class);
	}

}
