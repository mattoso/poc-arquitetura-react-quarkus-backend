package br.org.reactjs.kafka.frontend.states;

import br.org.reactjs.kafka.base.view.ViewState;

/**
 * ViewState utilizado na pagina Home do sistema.
 * 
 * @author Theodoro Mattoso
 * @since 26/12/2020
 */
public class HomeViewState implements ViewState {

	private String viewId;
	private String color;
	
	public HomeViewState() {
		this.viewId = "6fa183002902";
		this.color = "#000";
	}

	/**
	 * @return the color
	 */
	public String getColor() {
		return this.color;
	}

	/**
	 * @param color the color to set
	 */
	public void setColor(String color) {
		this.color = color;
	}

	@Override
	public String getViewId() {
		return this.viewId;
	}

	@Override
	public void setViewId(String viewId) {
		this.viewId = viewId;
	}

}