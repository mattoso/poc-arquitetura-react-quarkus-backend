package br.org.reactjs.kafka.frontend;

import io.quarkus.kafka.client.serialization.ObjectMapperSerializer;

/**
 * Serializador de mensagens de atualização de estados.
 * 
 * @author Theodoro Mattoso
 * @since 26/12/2020
 */
public class ViewStateSerializer extends ObjectMapperSerializer<String> {

}