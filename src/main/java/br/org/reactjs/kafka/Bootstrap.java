package br.org.reactjs.kafka;

import static br.org.reactjs.kafka.backend.ActionControllers.ACTION_CONTROLLERS;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

import br.org.reactjs.kafka.backend.controller.HomePageController;
import br.org.reactjs.kafka.base.constants.KafkaProps;
import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;

@ApplicationScoped
public class Bootstrap {
	
	@Inject
	protected KafkaProps kafkaProps;

	void onStart(@Observes StartupEvent ev) {
		ACTION_CONTROLLERS.registerController(HomePageController.initialize(this.kafkaProps));
    }

    void onStop(@Observes ShutdownEvent ev) {
    }
	
}
